from .account_views import *
from .pay_views import *
from .cart_views import *
from .item_views import *
from .order_views import *

# viewsフォルダの中のファイルが呼び出された場合、「__init__.py」から呼び出される