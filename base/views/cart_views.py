from django.shortcuts import redirect, render
from django.conf import settings
from django.views.generic import View, ListView
from base.models import Item
from collections import OrderedDict
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required


class CartListView(LoginRequiredMixin, ListView):
    """
    カートページを開く処理
    """
    model = Item
    template_name = 'pages/cart.html'

    # get_queryset: ListViewのメソッド　オーバライドしている
    # 最終的にget_querysetをreturnする
    # https://qiita.com/aqmr-kino/items/d536e08a715a9fad5720
    def get_queryset(self):
        """
        商品ごとのクエリーセットの取得、要素の追加
        ・カートに入っている「商品ごと」にクエリーセットを取得して、
        　「商品ごと」のクエリーセットの要素に「quantity」と「subtotal」を追加
        """
        cart = self.request.session.get('cart', None)
        if cart is None or len(cart) == 0:
            return redirect('/')

        self.queryset = []
        self.total = 0
        for item_pk, quantity in cart['items'].items():
            obj = Item.objects.get(pk=item_pk)
            # print(obj)
            # print(vars(obj))
            obj.quantity = quantity
            obj.subtotal = int(obj.price * quantity)
            self.queryset.append(obj)
            self.total += obj.subtotal

        self.tax_included_total = int(self.total * (settings.TAX_RATE + 1))
        # カートの値を更新
        cart['total'] = self.total
        cart['tax_included_total'] = self.tax_included_total
        self.request.session['cart'] = cart

        # print('get_queryset中身S')
        # query = super().get_queryset()
        # for object in query:
        #     print(vars(object))
        # print(super().get_queryset())
        # print('get_queryset中身E')

        # 親のquerysetを返す　self.querysetがある場合、返すことができる
        return super().get_queryset()


    # get_context_data: ListViewのメソッド
    def get_context_data(self, **kwargs):
        """
        フロント側に返す辞書型のデータ「context」の作成
        ・上のget_querysetメソッドで作成したクエリーセット(return super().get_queryset()したもの)は、
        　辞書contextのキー「object_list」に格納済
        ・？（return super().get_queryset() == context['object_list']）
        ・辞書contextにキー「total」と「tax_included_total」を追加して、対応する値を格納
        """
        context = super().get_context_data(**kwargs)
        try:
            # contextの中にキー「total」「tax_included_total」とそれぞれの値を追加
            context['total'] = self.total
            context['tax_included_total'] = self.tax_included_total
        except Exception:
            pass

        # print('context中身S')
        # print(context)
        # print(context['object_list'])
        # for object in context['object_list']:
        #     print(vars(object))
        # print('context中身E')

        return context


class AddCartView(LoginRequiredMixin, View):
    """
    カートに商品を入れる処理
    """
    def post(self, request):
        item_pk = request.POST.get('item_pk')
        quantity = int(request.POST.get('quantity'))

        # 第一引数が指定されなかった場合、第二引数で定めた値を格納
        cart = request.session.get('cart', None)
        # カートに何も入っていない場合
        if cart is None or len(cart) == 0:
            # OrderedDict 設定された要素の順番を保持
            items = OrderedDict()
            cart = {'items': items}

        if item_pk in cart['items']:
            cart['items'][item_pk] += quantity
        else:
            cart['items'][item_pk] = quantity
        request.session['cart'] = cart

        return redirect('/cart/')


"""
login_required:ログインしていないとこの関数を使用できない
LoginRequiredMixinと同様
"""
@login_required
def remove_from_cart(request, pk):
    """
    カートに入っている商品を削除する処理
    """
    cart = request.session.get('cart', None)
    if cart is not None:
        del cart['items'][pk]
        request.session['cart'] = cart
    return redirect('/cart/')


############################################
# カートセッション削除用
def session_clear(request):
    del request.session['cart']
    print('セッション削除')
    return redirect('/')
############################################