from pickle import OBJ
from django.views.generic import ListView, DetailView
from base.models import Order
import json
from django.contrib.auth.mixins import LoginRequiredMixin


class OrderIndexView(LoginRequiredMixin, ListView):
    """
    注文履歴の一覧画面を開く
    """
    model = Order
    template_name = 'pages/orders.html'
    # 並び替え
    ordering = '-created_at'

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user)


class OrderDitailView(LoginRequiredMixin, DetailView):
    """
    個別の注文画面を開く
    """
    model = Order
    template_name = 'pages/order.html'

    # get_context_dataメソッドをオーバーライドすることで、
    # テンプレートに渡すコンテキストを加工
    def get_context_data(self, **kwargs):
        # dbに保存されているJson形式のデータをテンプレート側で表示できるように加工
        # super().get_context_data(**kwargs)で取得する値は、通常はそのままreturnするが、ここでは加工する
        context = super().get_context_data(**kwargs)
        # DetailViewのためPKで取得している１つのオブジェクト　URLからPKの値を取得している
        obj = self.get_object()
        # Jsonデータをpythonの辞書型に変換
        context['items'] = json.loads(obj.items)
        context['shipping'] = json.loads(obj.shipping)
        return context