from django.shortcuts import render
from django.views.generic import ListView, DetailView
from base.models import Item


# 全てのItemを取得
class IndexListView(ListView):
    model = Item
    template_name = 'pages/index.html'

"""
上の「class IndexListView(ListView)」は下記のことと同じ

def index(request):
    object_list = Item.objects.all()
    context = {
        'object_list': object_list
    }
    return render(request, 'pages/index.html', context)
"""

# 個別のItemを取得のみを取得（pk(id)をもとに取得）
class ItemDetailView(DetailView):
    model = Item
    template_name = 'pages/item.html'