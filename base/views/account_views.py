from django.views.generic import CreateView, UpdateView
from django.contrib.auth.views import LoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from base.models import Profile
from base.forms import UserCreationForm


# super()は基底クラスのメソッドを継承した上で処理を拡張させたい時に使う
# 子クラスで親クラスの持っているメソッドを書き換えることをオーバーライド

class SingUpView(CreateView):
    """
    新規登録
    """
    form_class = UserCreationForm
    # 新規登録が成功した場合に飛ばすページ
    success_url = '/login/'
    template_name = 'pages/login_siginup.html'

    # データがポストされた時に呼ばれるメソッド
    def form_valid(self, form):
        return super().form_valid(form)


class Login(LoginView):
    """
    ログイン
    """
    template_name = 'pages/login_siginup.html'

    # 入力された値に問題なく、バリデーションを通った時にこの関数が呼ばれる
    # 正常系の処理を実装
    def form_valid(self, form):
        return super().form_valid(form)

    # バリデーションに引っかかった場合に呼ばれるメソッド
    # 異常系の処理を実装
    def form_invalid(self, form):
        return super().form_invalid(form)


# LoginRequiredMixinを継承する場合、必ず第１引数に書く
# UpdateViewは、既に登録されているデータを開き、更新するためのビュー
class AccountUpdateView(LoginRequiredMixin, UpdateView):
    """
    Userの更新
    """
    model = get_user_model()
    template_name = 'pages/account.html'
    # 入力したいフィールド
    fields = ('username', 'email')
    success_url = '/account/'

    # 単一のオブジェクトを返す
    def get_object(self):
        # success_url = '/account/' のため、更新時にどのUserを対象にするか不明
        # URL変換ではなく、現在のユーザーから直接pkを取得
        self.kwargs['pk'] = self.request.user.pk
        return super().get_object()


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    """
    Profileの更新
    """
    model = Profile
    template_name = 'pages/profile.html'
    fields = ('name', 'zipcode', 'prefecture', 'city', 'address1', 'address2', 'tel')
    success_url = '/profile/'

    def get_object(self):
        # URL変換ではなく、現在のユーザーから直接pkを取得
        self.kwargs['pk'] = self.request.user.pk
        return super().get_object()

        # 子クラスのself.kwargs['pk']をスーパークラスの「self」で受け取っている
        # スーパークラス
        # def get_object(self, queryset=None):
            # pk = self.kwargs.get(self.pk_url_kwarg)

        """
        ・super().親クラスのメソッド # python3系での標準の書き方
        ・super(親クラスのオブジェクト, self).親クラスのメソッド # python2系での書き方
        python2系での書き方のように、super()の引数として親クラスのオブジェクトとselfを持つ書き方が標準でしたが、
        3系になり「省略」する形が標準となりました
        """