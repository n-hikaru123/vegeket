from django import forms
# このプロジェクトで設定されているUserモデルを取ってくる
from django.contrib.auth import get_user_model


# モデルを利用したフォームクラス
class UserCreationForm(forms.ModelForm):
    """
    ユーザーを作成するフォームの作成
    """
    password = forms.CharField()

    class Meta:
        model = get_user_model()
        fields = ('username', 'email', 'password', )

    def clean_password(self):
        # cleaned_data：「あるオブジェクトの属性値の中でバリデーションをクリアしたものだけを辞書形式で格納したもの」
        password = self.cleaned_data.get('password')
        return password

    def save(self, commit=True):
        user = super().save(commit=False)
        # set_passwordはハッシュされたパスワードを作成
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user