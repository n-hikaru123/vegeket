from base.models.order_models import Order
from base.forms import UserCreationForm
from django.contrib import admin
from base.models import Item, Category, Tag, User, Profile, Order
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin

# 管理画面の操作の設定


# TabularInlineサブクラスを使って項目詳細を設定
class TagInline(admin.TabularInline):
    model = Item.tags.through


class ItemAdmin(admin.ModelAdmin):
    # 新しく表示する項目
    inlines = [TagInline]
    # 除外する項目（元々の項目）
    exclude = ['tags']


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False


# 管理画面でUserモデルとして画面に表示するため、どの項目を表示するか指定
class CustomUserAdmin(UserAdmin):
    # 各userの詳細画面に表示される項目
    fieldsets = (
        # ここでは２段に分けて書いているが、本来は必要ない。画面上で２段に分かれるため見やすくした
        (None, {'fields': ('username', 'email', 'password', )}),
        (None, {'fields': ('is_active', 'is_admin', )})
    )

    # user画面のファーストヴューの一覧に表示されるリストの項目
    list_display = ('username', 'email', 'is_active')
    # 指定したフィールドの絞り込みができる
    list_filter = ()
    # オブジェクトのデフォルトの並び方を変更す
    ordering = (['username'])
    # 複数選択ボックス
    filter_horizontal = ()

    add_fieldsets = (
        (None, {'fields': ('username', 'email', 'is_active', )})
    )

    # UserCreationFormは既に自己で作成した入力フォームで使い回している（管理画面上で新しくユーザーを作成）
    add_form = UserCreationForm
    # UserのページにセットでProfileを表示するようにする
    # 管理画面上でUserの情報を見るとProfileの情報も見ることができる
    # このプロジェクトではUserモデルとProfileモデルを１対１の関係にしている(OneToOneField)
    inlines = (ProfileInline, )


# 管理画面で対象テーブルの操作ができるようになる
admin.site.register(Item, ItemAdmin)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Order)
# 管理画面にデフォルトで表示されている「グループ」を非表示にする
admin.site.unregister(Group)

