import os.path

from django.db import models
from django.utils.crypto import get_random_string


# 22文字のランダムの文字列を生成
def create_id():
    return get_random_string(22)


def upload_image_to(instance, filename):
    # instance.id:ここではitemのid 自動的に振り分けられる22文字の文字列
    item_id = instance.id
    return os.path.join('static', 'items', item_id, filename)


class Tag(models.Model):
    slug = models.CharField(max_length=32, primary_key=True)
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Category(models.Model):
    slug = models.CharField(max_length=32, primary_key=True)
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Item(models.Model):
    # editable = False：管理画面上で変更できないようにする
    id = models.CharField(default=create_id, primary_key=True, max_length=22, editable=False)
    name = models.CharField(default='', max_length=50)
    # PositiveIntegerField：正の整数（マイナスの値は扱わない）
    price = models.PositiveIntegerField(default=0)
    stock = models.PositiveIntegerField(default=0)
    # editable=''：登録時に空でも可、blank = True：更新時に空でも可
    description = models.TextField(editable='', blank=True)
    sold_count = models.PositiveIntegerField(default=0)
    # is_published：ここではTureの時に公開
    is_published = models.BooleanField(default=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    # upload_to：どこのファイルにアップロードされるのか
    image = models.ImageField(default='', blank=True, upload_to=upload_image_to)
    # Categoryが参照先のモデル
    # 参照している属するカテゴリーが削除された場合、NULLをセットする
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    # 一アイテムに対して複数のタグを設定
    # ManyToManyField：複数設定することが可能（中間テーブル）
    tags = models.ManyToManyField(Tag)

    # このオブジェクトを参照した時にnameのみが返される
    # インスタンスが何か分かりやすくするために使う
    def __str__(self):
        return self.name