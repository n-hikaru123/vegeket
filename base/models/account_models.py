from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from base.models import create_id


# https://docs.djangoproject.com/ja/3.2/topics/auth/customizing/#a-full-example
# https://office54.net/python/django/model-custom-user

# UserManagerクラスには_create_user関数とcreate_user関数、create_superuserの３つがある。
# それぞれユーザーを新規で作成する際に呼ばれる関数
class UserManager(BaseUserManager):
    """
    djangoが標準で持っているusermodelをカスタマイズする
    既存に持っているフィールドの他にフィールドを追加するなど
    """
    def create_user(self, username, email, password=None):
        """
        一般ユーザー
        """
        if not email:
            # 空の場合は例外が発生
            raise ValueError('Users must have an email ddress')
        user = self.model(
            username=username,
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password=None):
        """
        pythonのスーパーユーザー
        """
        user = self.create_user(
            username,
            email,
            password
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


# AbstractBaseUserには認証機能のみが含まれており、独自でフィールドを設定することができる
class User(AbstractBaseUser):
    id = models.CharField(default=create_id, primary_key=True, max_length=22)
    username = models.CharField(
        max_length=50, unique=True, blank=True, default='匿名'
    )
    email = models.EmailField(max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    # 管理画面にログインできるかどうか
    is_admin = models.BooleanField(default=False)
    objects = UserManager()
    # USERNAME_FIELDで指定したフィールドは、ログイン認証やメール送信などで利用
    USERNAME_FIELD = 'username'
    # ターミナルでユーザー作成（manage.py createsuperuser）するときに表示される項目
    # ユーザーは指定した項目の値を入力するよう求められる
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email', ]

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app 'app_label'?"
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: ALL admins are staff
        return self.is_admin


# ここでは、userモデルがコアな機能としてProfileモデルと分けた
# userモデルをメイン、Profileモデルをサブとした
class Profile(models.Model):
    # OneToOneField　１対１で紐づける Userモデルと１対１の関係
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    name = models.CharField(default='', blank=True, max_length=50)
    zipcode = models.CharField(default='', blank=True, max_length=50)
    prefecture = models.CharField(default='', blank=True, max_length=50)
    city = models.CharField(default='', blank=True, max_length=50)
    address1 = models.CharField(default='', blank=True, max_length=50)
    address2 = models.CharField(default='', blank=True, max_length=50)
    tel = models.CharField(default='', blank=True, max_length=15)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


# ProfileモデルはUserモデルと１対１の関係　OneToOneField
# Userモデルが作成された時にProfileモデルを作成
# @:デコレーター　ここではdef create_onetooneを実行する前に、@receiver(post_save, sender=User)の処理を行う
# Userが作られて保存された後にcreate_onetooneが実行される(post_save)
@receiver(post_save, sender=User)
def create_onetoone(sender, **kwargs):
    if kwargs['created']:
        Profile.objects.create(user=kwargs['instance'])
