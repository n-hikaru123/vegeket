"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# ここではアプリケーション管理フォルダでurlを一元管理

from django.contrib import admin
from django.urls import path
from stripe import Account
from base import views
# ログアウト
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('admin/', admin.site.urls),

    # Account
    path('login/', views.Login.as_view()),
    # viewsファイルではなく、直接ここでLogout機能を実装
    path('logout/', LogoutView.as_view()),
    path('signup/', views.SingUpView.as_view()),
    path('account/', views.AccountUpdateView.as_view()),
    path('profile/', views.ProfileUpdateView.as_view()),

    # Order
    path('orders/<str:pk>/', views.OrderDitailView.as_view()),
    path('orders/', views.OrderIndexView.as_view()),

    # Pay
    path('pay/checkout/', views.PayWithStripe.as_view()),
    path('pay/success/', views.PaySuccessView.as_view()),
    path('pay/cancel/', views.PayCancelView.as_view()),

    # Cart
    path('cart/remove/<str:pk>/', views.remove_from_cart),
    path('cart/add/', views.AddCartView.as_view()),
    path('cart/', views.CartListView.as_view()), # カートページ

    # Items
    path('items/<str:pk>/', views.ItemDetailView.as_view()),

    path('', views.IndexListView.as_view()), # トップページ


    ############################################
    # セッション削除用
    path('session_clear', views.session_clear)
    ############################################
]


"""
クラスviewを呼び出す場合は、as_view()が必要
関数viewを呼び出す場合は、as_view()が不要
"""
